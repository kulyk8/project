function openTab(tabId) {
  const tabContents = document.getElementsByClassName("services_info");
  for (const content of tabContents) {
    content.style.display = "none";
  }

  const activeTab = document.getElementById(tabId);
  activeTab.style.display = "flex";

  const worksElements = document.querySelectorAll(".services");
  worksElements.forEach((element) => {
    if (element.id === tabId) {
      element.classList.add("active"); 
    } else {
      element.classList.remove("active"); 
    }
  });
}

const allImagesContainer = document.querySelector(".image_container");
const hiddenImagesContainer = document.querySelector(".hidden_works");

const allCategories = [
  "all",
  "graphic_design",
  "web_design",
  "landing_pages",
  "wordpress",
];
let currentCategory = "all";

function loadMoreImages() {
  const imagesPerPage = 12;
  const totalImages = hiddenImagesContainer.children.length;

  for (let i = 0; i < imagesPerPage && i < totalImages; i++) {
    const image = hiddenImagesContainer.children[i].cloneNode(true);
    allImagesContainer.appendChild(image);
  }

  hiddenImagesContainer.children =
    hiddenImagesContainer.children.slice(imagesPerPage);

  if (hiddenImagesContainer.children.length === 0) {
    document.querySelector(".plus_in_button").style.display = "none";
  }
}

function filterImages(category) {
  currentCategory = category;

  for (const image of allImagesContainer.children) {
    const imageCategory = image.dataset.category;
    if (category === "all" || imageCategory === category) {
      image.style.display = "block";
    } else {
      image.style.display = "none";
    }
  }
}

function resetFilter() {
  filterImages("all");
  const worksElements = document.querySelectorAll(".works");
  worksElements.forEach((element) => {
    element.style.border = "1px solid #DADADA";
  });
}

document.querySelector(".work_one").addEventListener("click", () => {
  resetFilter();
  filterImages("all");
});

document.querySelector(".work_two").addEventListener("click", () => {
  resetFilter();
  filterImages("graphic_design");
});

document.querySelector(".work_three").addEventListener("click", () => {
  resetFilter();
  filterImages("web_design");
});

document.querySelector(".work_four").addEventListener("click", () => {
  resetFilter();
  filterImages("landing_pages");
});

document.querySelector(".work_five").addEventListener("click", () => {
  resetFilter();
  filterImages("wordpress");
});

const worksElements = document.querySelectorAll(".works");

let selectedBlock = null;

worksElements.forEach((element) => {
  element.addEventListener("click", () => {
    if (selectedBlock !== element) {
      if (selectedBlock) {
        selectedBlock.style.border = "1px solid #DADADA";
      }
      element.style.border = "1px solid #18CFAB";
      selectedBlock = element;
    }
  });
});

$(document).ready(function () {
  const testimonials = $(".about_block");
  const miniPhotos = $(".mini_photo_area");
  const prevButton = $(".carousel_button.prev");
  const nextButton = $(".carousel_button.next");

  let currentIndex = 0;

  function showSlide(index) {
    testimonials.hide().eq(index).fadeIn();
    miniPhotos.removeClass("active");
    miniPhotos.eq(index).addClass("active");
  }

  miniPhotos.on("click", function () {
    const clickedIndex = $(this).index();
    showSlide(clickedIndex);
  });

  prevButton.on("click", function () {
    currentIndex =
      (currentIndex - 1 + testimonials.length) % testimonials.length;
    showSlide(currentIndex);
  });

  nextButton.on("click", function () {
    currentIndex = (currentIndex + 1) % testimonials.length;
    showSlide(currentIndex);
  });

  showSlide(currentIndex);
});
